(function ($) {
    $(function () {
	$('.modal').modal();
	$('#modal1').modal('open');
    });
})(jQuery);


var sort = new Vue ({
    el: '#sort',
    data: {
	message: '',
	memberList:[
	    {no:1, name:'A さん', img:'img/ikaku.png', categoryNo:'1'},
	    {no:2, name:'B さん', img:'img/myo-n.png', categoryNo:'2'},
	    {no:3, name:'C さん', img:'img/shirokuro.jpg', categoryNo:'3'},
	    {no:4, name:'D さん', img:'img/goron.png', categoryNo:'4'},
	    {no:5, name:'E さん', img:'img/ikaku.png', categoryNo:'1'},
	    {no:6, name:'F さん', img:'img/myo-n.png', categoryNo:'2'},
	    {no:7, name:'G さん', img:'img/goron.png', categoryNo:'3'},
	    {no:8, name:'H さん', img:'img/shirokuro.jpg', categoryNo:'4'},
	    {no:9, name:'dummy', img:'img/shirokuro.jpg', categoryNo:'1'}
	],
	deleteList:[
	]
    },
    methods: {
//	deleteMember: function(member, index){
//	    this.memberList.splice(index, 1);
//	},

	deleteMember: function(member, index){
	    this.deleteList.unshift(this.memberList[index]);
	    this.memberList.splice(index, 1);
	},

	undoMember: function(dList, index){
	    this.memberList.push(this.deleteList[index]);
	    this.deleteList.splice(index, 1);
	},
	
	deleteDummy: function(){
	    this.memberList.pop();
	},

	test: function(){
	    this.message = this.memberList;
	}

	

	
    }
});
